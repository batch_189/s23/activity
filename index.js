/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function
*/

    let myPokemon = {
        name: "Pikachu",
        level: 3,
        health: 100,
        attack: 50,
        tackle: function(){
            
            console.log("This pokemon tackled targetPokemon ");
            console.log("targetPokemon's health is now reduced to targetPokemonHealth");
        },
        faint: function(){
            console.log("Pokemon fainted");
        },
    };
    // console.log(myPokemon);


    function PokemonBattle (name, level) {
        
        // Properties
        this.name = name;
        this.level = level;
        this.health = 5 * level;
        this.attack = level;
        
        //method
        this.tackle = function (target){

            console.log(this.name + " tackled " + target.name);
            console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
            target.health = (target.health - this.attack);
            
        }
        console.log("yoy")
        this.faint = function (target) {
        
            if ( target.health <= 5 ){
                
                return "You don't gave enough life";
            } else {

                return "You still have enough life";
            }
        }

    };

    let chamander = new PokemonBattle ("chamander", 20);
    let chatot = new  PokemonBattle ( "chatot", 10);

    console.log(chamander);
    console.log(chatot);
    
    chamander.tackle(chatot);
    chamander.tackle(chatot);
    
   let returnMessage = chamander.faint(chatot);
   console.log(returnMessage);